#!/usr/bin/env python3
#coding : utf-8
import socketio
import yaml

from game import Game

class Lobby:

    def __init__(self, id, host, hostname, lobbyName, rules_file = 'static/rules.yml'):
        self.id = id
        self.players = {}
        self.host = host
        self.players[host] = hostname
        self.lobbyName = lobbyName
        self.game_started = False
        self.game = None
        with open(rules_file) as r:
            rules = yaml.safe_load(r)
            self.rules = rules['rules']['default_rules']

    def addPlayer(self, playerSid, playerName):
        self.players[playerSid] = playerName
        if self.game:
            self.game.players.append(playerSid)

    def removePlayer(self, playerSid):
        del self.players[playerSid]
        if self.game:
            self.game.players.remove(playerSid)

    def updateRules(self, newRules):
        for rule in newRules:
            self.rules[rule] = newRules[rule]

    def startGame(self):
        self.game = Game(self.rules, list(self.players.keys()))

    def startNextRound(self):
        self.game.round_start()

    def getGamePlayers(self):
        return self.game.players

    def getWord(self, sid):
        return self.game.players_word[sid]

    def getRole(self, sid):
        return self.game.players_role[sid]

    def getPlayerTurn(self):
        return self.game.players[self.game.player_turn]

    def getScore(self):
        return self.game.scores

    def getWordsSent(self):
        return self.game.word_sent