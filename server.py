#!/usr/bin/env python3
#coding : utf-8

from aiohttp import web
import socketio
from lobby import Lobby
import uuid

sio = socketio.AsyncServer(cors_allowed_origins='*',logger=True) #   engineio_logger=True
app = web.Application()
sio.attach(app)
listLobby = {}

# static_files = {
#
# }

# basic socketio connection event
@sio.event
async def connect(sid, environ): #test --> ok
    print('connect ', sid)

@sio.event
async def disconnect(sid): #test --> ok
    print('disconnect', sid)
    session = await sio.get_session(sid)
    if 'inLobby' in session:
        lobbyDisconnect(sid, session['inLobby'])

#set username for a client
@sio.event
async def setUsername(sid, username):
    await sio.save_session(sid, {'username': username})

# A client want to create a lobby
@sio.event
async def lobbyNew(sid, nomLobby): #test --> client1 --> ok
    newId = uuid.uuid1()
    idLobby = newId.hex
    session = await sio.get_session(sid)
    hostname = session['username']
    print(hostname)
    newLobby = Lobby(idLobby,sid,hostname,nomLobby)
    listLobby[idLobby] = newLobby
    lobby = listLobby[idLobby]
    sio.enter_room(sid, idLobby)
    print(lobby.players.keys())
    print(lobby.players.values())
    await sio.save_session(sid, {'inLobby': idLobby})
    await sio.emit('lobbyJoined',{'players': list(lobby.players.keys()), 'pseudo' : list(lobby.players.values()), 'rules': lobby.rules, 'host': lobby.host, 'lobbyId': lobby.id}, room=sid)

# A client want to join a lobby
@sio.event
async def lobbyJoin(sid, idLobby): #test --> client2 --> ok
    sio.enter_room(sid, idLobby)
    lobby = listLobby[idLobby]
    session = await sio.get_session(sid)
    name = session['username']
    lobby.addPlayer(sid, name)
    print(lobby.players)
    await sio.save_session(sid, {'inLobby': idLobby})
    await sio.emit('lobbyJoined', {'players': list(lobby.players.keys()), 'pseudo' : list(lobby.players.values())  , 'rules': lobby.rules, 'host': lobby.host, 'lobbyId': lobby.id}, room=sid)
    await sio.emit('playerJoined', {'player': sid}, room=idLobby, skip_sid=sid) #alert all the lobby that a new player is joining

# A client request the list of all lobby
@sio.event
async def getLobbyList(sid): #test --> client2 --> ok
    print(listLobby)
    await sio.emit('lobbyList', {'list': list(listLobby.keys())}, room=sid)


# # A client request the players of a lobby
# @sio.event
# async def getLobbyPlayers(sid, idLobby): #test --> ok
#     await sio.emit('lobbyPlayers', {'players': listLobby[idLobby].players}, room=sid)

# @sio.event
# async def getIdLobby(sid):
#     session = await sio.get_session(sid)
#     if 'inLobby' in session:
#         await sio.emit('sendlobbyId',session['inLobby'])

# A client disconnect from a lobby
@sio.event
async def lobbyDisconnect(sid, idLobby):
    sio.leave_room(sid, idLobby)
    lobby = listLobby[idLobby]
    lobby.removePlayer(sid)
    #todo : suppr session inLobby
    with sio.session(sid) as session:
        del session['inLobby']
    if len(lobby.players.keys()) == 0: # delete the lobby if it is empty
        del listLobby[idLobby]
    elif lobby.host == sid: # potential host changment if the host leave
        lobby.host = lobby.players.keys()[0]

# A client change rules
@sio.event
async def lobbySetRules(sid, newRules, idLobby): #test --> client1 --> ok
    print("old rules:")
    print(listLobby[idLobby].rules)
    listLobby[idLobby].updateRules(newRules)
    await sio.emit('lobbyRulesUpdated', {'rules': listLobby[idLobby].rules}, room=idLobby) #send, to all client in the lobby, the new rules

# A client request the rules
# @sio.event
# async def lobbyGetRules(sid, idLobby): #test --> client1 --> ok
#     await sio.emit('lobbyRulesUpdated', {'rules': listLobby[idLobby].lobbyRules}, room=sid) #send, to all client in the lobby, the new rules

# A host player start the game
@sio.event
async def startGame(sid, idLobby): # test --> client1/2 --> ok 
    lobby = listLobby[idLobby]
    if lobby.host == sid:
        lobby.startGame()
        await sio.emit('lobbyGameStarted', room=idLobby) #send, to all client in the lobby, the new rules
        lobby.startNextRound()
        for player in lobby.players.keys(): # For all players in the game, we send the players order, his word and his role
            await sio.emit('roundStarted', {'players': lobby.getGamePlayers(), 'your_word': lobby.getWord(player), 'your_role': lobby.getRole(player)} , room=player)
        await sio.emit('playerTurn', lobby.getPlayerTurn(),room=idLobby) # send the first Turn

# A client request the scores
@sio.event
async def getScore(sid, idLobby): # a delete --> le serv doit renvoyer le score quand il est actualisé
    await sio.emit('scores',listLobby[idLobby].getScore(),room=sid)

@sio.event
async def sendWord(sid, idLobby, word): # test --> client 1/2 --> ok
    lobby = listLobby[idLobby]
    lobby.game.sendWord(sid,word)
    await sio.emit('wordSent', lobby.getWordsSent(), room=idLobby)
    if lobby.game.isEndRound(): #if this is the end of the round, we start the vote phase
        await sio.emit('votePhase', room=idLobby)
    else:
        await sio.emit('playerTurn', lobby.getPlayerTurn(),room=idLobby) # send the first Turn

@sio.event
async def votePlayer(sid, playerVoted, idLobby):  # test --> client 1/2 --> ok
    lobby = listLobby[idLobby]
    lobby.game.vote(sid, playerVoted)
    await sio.emit('playerVoted', {'player_id' : sid, 'voted_by' : playerVoted}, room=idLobby, skip_sid=sid) # send to all but not the sid
    if lobby.game.allPlayerVoted():
        await sio.emit('endRound', {'score':lobby.game.getRoundResult(), 'undercover': lobby.game.round_undercover,'undercover_word':lobby.game.round_undercover_word, 'innocent_word':lobby.game.round_innocent_word}, room=idLobby)
        if lobby.game.isEndGame():
            await sio.emit('endGame', lobby.game.scores(), room=idLobby)
            lobby.game = None
        else:
            lobby.startNextRound()
            for player in lobby.players.keys(): # For all players in the game, we send the players order, his word and his role
                await sio.emit('roundStarted', {'players': lobby.getGamePlayers(), 'your_word': lobby.getWord(player), 'your_role': lobby.getRole(player)} , room=player)
            await sio.emit('playerTurn', lobby.getPlayerTurn(),room=idLobby) # send the first Turn

async def test_client(request):
    with open('test_client/client.html') as f:
        return web.Response(text=f.read(), content_type='text/html')

async def test_client2(request):
    with open('test_client/client2.html') as f:
        return web.Response(text=f.read(), content_type='text/html')

async def test_index(request):
    with open('test_client/index.html') as f:
        return web.Response(text=f.read(), content_type='text/html')

app.router.add_get('/client1', test_client)
app.router.add_get('/client2', test_client2)
app.router.add_get('/', test_index)

if __name__ == '__main__':
    web.run_app(app)
