#!/usr/bin/env python3
#coding : utf-8
import random

class Game:

    def __init__(self, rules, players):
        self.players = players
        self.rules = rules
        self.current_round = 0
        self.current_word_round_step = 0
        self.words = [word.strip() for word in open("static/couples.txt", encoding="utf-8")]
        self.word_sent = {}
        self.scores = {}
        for i in range(len(players)):
            self.scores[players[i]] = 0
        self.players_word = {}
        self.players_role = {}
        self.playersVote = {}
        self.playersNumberVote = {}
        self.player_turn = 0
        self.round_innocent_word = ""
        self.round_undercover_word = ""

    def round_start(self):
        self.word_sent = {}
        self.current_round += 1
        self.current_word_round_step = 1
        line = random.choice(self.words).split(',')
        self.round_innocent_word = line[0]
        self.round_undercover_word = line[1]
        random.shuffle(self.players)
        for i in range(len(self.players)):
            self.players_word[self.players[i]] = line[0]
            self.players_role[self.players[i]] = 'innocent'
            self.word_sent[self.players[i]] = [] #list of all word sent by players in this round
        self.round_undercover = random.choice(self.players)
        self.players_word[self.round_undercover] = line[1]
        self.players_role[self.round_undercover] = 'undercover'
        self.player_turn = 0 # Offset of the current player turn
        self.playersVote = {}
        for player in self.players:
            self.playersNumberVote[player] = 0

    def isEndGame(self):
        if self.current_round == self.rules['nb_round']:
            return True
        return False

    def sendWord(self, player, word):
        if self.players[self.player_turn] == player:
            self.word_sent[player].append(word)
            self.player_turn += 1
            if (len(self.players) == self.player_turn): # if all players have sent word we continue with the first player again
                self.player_turn = 0
                self.current_word_round_step += 1

    def isEndRound(self):
        print('player_turn = '+str(self.player_turn))
        print('len player = '+str(len(self.players)))
        print('current word step : '+str(self.current_word_round_step))
        print('rules :'+str(self.rules['words_per_round']))
        if self.current_word_round_step == self.rules['words_per_round'] + 1:
            return True
        return False

    def vote(self, sid, playerVoted):
        self.playersVote[sid] = playerVoted
        self.playersNumberVote[playerVoted] += 1

    def allPlayerVoted(self):
        for player in self.players :
            if player not in self.playersVote :
                return False
        return True

    def getRoundResult(self):
        #pont Innocent = (nombre de joueurs * 100 ) / 2
        #points undercover = ( (nombre de joueurs * 100) / 1.5 ) - (nombre de vote contre lui * 100 )
        #innoncent qui vote seul = (nombre de joueurs * 100 ) / 2 + (nombre de joueurs * 100) / 5
        nb_players = len(self.players)
        for player in self.players :
            if self.players_role[player] == "undercover" :
                nb_vote_undercover = 0
                for vote in self.playersVote.values() :
                    if vote == player :
                        nb_vote_undercover += 1
                undercover_score = ((nb_players * 100) / 1.5) - (nb_vote_undercover * 100)
                if undercover_score > 0 :
                    self.scores[player] += undercover_score
            else:
                if self.playersVote[player] == self.round_undercover :
                        if self.voted_alone(player):
                            self.scores[player] += ((nb_players * 100) / 2) + ((nb_players *100) / 5)
                        else:
                            self.scores[player] += (nb_players * 100) / 2
        return self.scores
        
    def voted_alone(self, player):
        playerVote = self.playersVote[player]
        for key,value in self.playersVote.items() :
            if key is not player and value == playerVote :
                return False
        return True
