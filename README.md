# Undercover

Server du jeu undercover en développement 

## But 

Tout le monde possède le même mot sauf une personne: c'est l'imposteur. L'imposteur lui même ne sait pas si il est l'imposteur, chacun à votre tour écrivez un mot pour prouver que vous n'êtes pas l'imposteur. L'imposteur gagne si il n'est pas le plus voté à la fin, sinon les autres joueurs gagnent. 